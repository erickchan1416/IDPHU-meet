function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

var session = getParameterByName('session');
// console.log('session', session);
const resource = "meet.jit.si";
var w = window.innerWidth ||
    document.documentElement.clientWidth ||
    document.body.clientWidth;

var h = window.innerHeight ||
    document.documentElement.clientHeight ||
    document.body.clientHeight;
console.log('width', w);

const specs = {
    roomName: session,
    width: w,
    height: 900,
    parentNode: document.querySelector('#meet'),
    configOverwrite: {
        defaultLanguage: 'de',
        enableClosePage: false
    },
    interfaceConfigOverwrite: {
        APP_NAME: 'IDPHU Meet',
        NATIVE_APP_NAME: 'IDPHU Meet',
        PROVIDER_NAME: 'IDPHU',
        CLOSE_PAGE_GUEST_HINT: true,
        filmStripOnly: false,
        SHOW_JITSI_WATERMARK: false,
        SHOW_WATERMARK_FOR_GUESTS: false,
        TOOLBAR_BUTTONS: [
            'microphone', 'camera', 'closedcaptions', 'desktop', 'fullscreen',
            'fodeviceselection', 'hangup', 'profile', 'info', 'chat', 'recording',
            'livestreaming', 'etherpad', 'settings', 'raisehand',
            'videoquality', 'filmstrip', 'invite', 'feedback', 'stats', 'shortcuts',
            'tileview', 'videobackgroundblur', 'download', 'help', 'mute-everyone'
        ],
    }
}
const api = new JitsiMeetExternalAPI(resource, specs);

event = 'videoConferenceLeft';
api.addEventListener(event, (data) => {
    // console.log('Saliendo', data);
    window.close();
});