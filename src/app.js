console.log('By GacelaJS');

async function hello() {

    const { value: session } = await Swal.fire({
        title: 'Por favor, ingresa el nombre de la sala.',
        input: 'text',
        showCancelButton: true,
        confirmButtonText: 'Entrar',
        cancelButtonText: 'Cancelar',
        inputValidator: (value) => {
            if (!value) {
                return 'Necesitas llenar este campo para poder continuar.'
            }
        }
    })

    if (session) {
        Swal.fire(`Se a redireccionado a ${session}`);
        window.open('https://www.colegioidphu.edu.co/aulavirtual/videollamada/session.html?session=' + session.trim());
    }
}